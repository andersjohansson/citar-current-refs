;;; citar-current-refs.el --- Add source for currently used references to citar  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Anders Johansson

;; Author: Anders Johansson <anders.l.johansson@chalmers.se>
;; Keywords: wp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'citar)
(require 'org)

(defcustom citar-current-refs-in-widened-buffer t
  "If non-nil, gathers current references from the widened buffer."
  :type 'boolean
  :group 'citar
  )

(defcustom citar-current-refs-in-all-org-buffers t
  "If non-nil, gathers current references from all org buffers."
  :type 'boolean
  :group 'citar)

(defvar citar-current-refs-fetching nil)

(defun citar-current-refs-fetch-org ()
  "Fetch refs in current org buffer."
  (let ((citar-current-refs-fetching t))
    (when-let ((keys (cl-remove-duplicates
                      (cl-loop for buf in (if citar-current-refs-in-all-org-buffers
                                              (org-buffer-list 'files t)
                                            (current-buffer))
                               append
                               (with-current-buffer buf
                                 (org-element-map
                                     (if citar-current-refs-in-widened-buffer
                                         (save-restriction
                                           (widen)
                                           (org-element-parse-buffer))
                                       (org-element-parse-buffer))
                                     'citation-reference
                                   (lambda (ref) (org-element-property :key ref)))))
                      :test 'equal))
               (cands (citar--get-candidates)))
      (cl-loop for key in keys
               collect (when-let ((found (seq-find
                                          (lambda (entry)
                                            (string-equal key (cadr entry)))
                                          (citar--get-candidates))))
                         (when (consp found)
                           (cons (propertize
                                  (concat
                                   (car found)
                                   ;; To make these entries unique (duplicates)
                                   (propertize "current" 'invisible t))
                                  'citar-current-refs t)
                                 (cdr found))))))))

(defun citar-current-refs-add-current-to-candidates (candidates)
  "Add refs in current buffer to CANDIDATES."
  (if citar-current-refs-fetching
      candidates
    (seq-concatenate 'list
                     (citar-current-refs-fetch-org)
                     candidates)))

(advice-add 'citar--get-candidates :filter-return #'citar-current-refs-add-current-to-candidates)

(defun citar-current-refs--group (cand transform)
  "Return group for CAND. TRANSFORM is not really used."
  (if transform
      cand
    (if (get-text-property 2 'citar-current-refs cand)
        "Current"
      "Rest")))

(advice-add 'citar--completion-table :override #'citar-current-refs--completion-table)
(advice-remove 'citar--completion-table #'citar-current-refs--completion-table)

(defun citar-current-refs--completion-table (candidates &optional filter)
  "Return a completion table for CANDIDATES.

CANDIDATES is an alist with entries (CAND KEY . ENTRY), where
  CAND is a display string for the bibliography item given
  by (KEY . ENTRY).

FILTER, if non-nil, should be a predicate function taking
  arguments KEY and ENTRY.  Only candidates for which this
  function returns non-nil will be offered for completion.

The returned completion table can be used with `completing-read`
and other completion functions."
  (let ((metadata `(metadata (category . citar-reference)
                             (affixation-function . ,#'citar--affixation)
                             (group-function . ,#'citar-current-refs--group))))
    (lambda (string predicate action)
      (if (eq action 'metadata)
          metadata
        (let ((predicate
               (when (or filter predicate)
                 (lambda (cand-key-entry)
                   (pcase-let ((`(,cand ,key . ,entry) cand-key-entry))
                     (and (or (null filter) (funcall filter key entry))
                          (or (null predicate) (funcall predicate cand))))))))
          (complete-with-action action candidates string predicate))))))


(provide 'citar-current-refs)
;;; citar-current-refs.el ends here
